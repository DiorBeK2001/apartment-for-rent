package uz.apartmentForRent.payload;

import lombok.Data;

@Data
public class ReqSignIn {
    private String userName;
    private String password;
}
