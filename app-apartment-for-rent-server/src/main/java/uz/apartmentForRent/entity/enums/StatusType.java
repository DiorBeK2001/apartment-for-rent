package uz.apartmentForRent.entity.enums;

public enum StatusType {
    EMPTY,       //bo'sh
    BUSY,        //band
    IN_PROGRESS  //zakaz ko'rib chiqilmoqda
}
