package uz.apartmentForRent.entity.enums;

public enum Timing {
    LONG_TIME,
    SHORT_TIME,
    PART_TIME
}
