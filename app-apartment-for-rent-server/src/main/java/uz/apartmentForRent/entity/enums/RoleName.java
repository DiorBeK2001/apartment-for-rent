package uz.apartmentForRent.entity.enums;

public enum RoleName {
    ROLE_SUPER_ADMIN, //systema egasi
    ROLE_ADMIN,       //admin YANI OPERATOR
    ROLE_SELLER,      //ijara beruvchi
    ROLE_USER         //oddiy foydallanuvchi yani ijarachi
}
