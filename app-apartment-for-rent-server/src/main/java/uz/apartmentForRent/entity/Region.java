package uz.apartmentForRent.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.apartmentForRent.entity.template.UuidNameEntity;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Region extends UuidNameEntity {

}
