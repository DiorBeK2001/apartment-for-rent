package uz.apartmentForRent.entity.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public abstract class UuidNameEntity extends UuidEntity {
    @Column(nullable = false, unique = true)
    private String name;

    private boolean active;

    @Column(columnDefinition = "text")
    private String description;
}
