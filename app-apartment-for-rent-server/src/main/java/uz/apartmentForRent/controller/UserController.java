package uz.apartmentForRent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.apartmentForRent.entity.User;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.payload.CheckPassword;
import uz.apartmentForRent.payload.UserDto;
import uz.apartmentForRent.security.CurrentUser;
import uz.apartmentForRent.service.UserService;

@RestController
@RequestMapping("/api/register")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/addSellerOrUser")
    public ApiResponse addUser(@RequestBody UserDto userDto, @RequestParam boolean seller) {
        return userService.addSellerOrUser(userDto, seller);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @PostMapping("/addAdmin")
    public ApiResponse addAdmin(@RequestBody UserDto userDto) {
        return userService.addAdmin(userDto);
    }

    @PutMapping("/editSellerOrUser")
    public ApiResponse editSellerOrUser(@RequestBody UserDto userDto) {
        return userService.editSellerOrUser(userDto);
    }


    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @PutMapping("/editAdmin")
    public ApiResponse editAdmin(@RequestBody UserDto userDto) {
        return userService.editAdmin(userDto);
    }

    @PostMapping("/editPassword")
    public ApiResponse editAdmin(@CurrentUser User user, @RequestBody CheckPassword checkPassword) {
        return userService.editPassword(user, checkPassword);
    }


}
