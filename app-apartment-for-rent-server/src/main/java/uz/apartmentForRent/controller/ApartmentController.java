package uz.apartmentForRent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.apartmentForRent.entity.User;
import uz.apartmentForRent.payload.ApartmentDto;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.security.CurrentUser;
import uz.apartmentForRent.service.ApartmentService;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/apartment")
public class ApartmentController {

    @Autowired
    ApartmentService apartmentService;

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN','ROLE_SELLER')")
    @PostMapping
    public ApiResponse saveOrEdit(@RequestBody ApartmentDto dto) {
        return apartmentService.saveOrEditApartment(dto);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @GetMapping("/{id}")
    public ApiResponse getApartment(@PathVariable UUID id) {
        return apartmentService.getApartment(id);
    }

    @GetMapping("/getApartmentList")
    public ApiResponse getApartmentList(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                        @CurrentUser User user) {
        return apartmentService.getAllApartment(page, size, user);
    }

    @PreAuthorize("hasAnyRole('ROLE_SELLER')")
    @GetMapping("/getAllApartmentsForSeller")
    public ApiResponse getAllApartmentsForSeller(@RequestParam(value = "page", defaultValue = "0") int page,
                                                 @RequestParam(value = "size", defaultValue = "10") int size,
                                                 @CurrentUser User user) {
        return apartmentService.getAllApartmentsForSeller(page, size, user);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN','ROLE_SELLER')")
    @GetMapping("/changeEnabled/{id}")
    public ApiResponse enabledApartment(@PathVariable UUID id) {
        return apartmentService.enabledApartment(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteApartment(@PathVariable UUID id) {
        return apartmentService.deleteApartment(id);
    }
}
