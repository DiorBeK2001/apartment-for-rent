package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.apartmentForRent.entity.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    AttachmentContent findByAttachmentId(UUID id);

    void deleteByAttachmentId(UUID attachment_id);
}
