package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.apartmentForRent.entity.Role;
import uz.apartmentForRent.entity.enums.RoleName;

import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Set<Role> findAllByRoleName(RoleName name);
}
