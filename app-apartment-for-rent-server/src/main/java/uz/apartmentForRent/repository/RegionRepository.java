package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.apartmentForRent.entity.Region;
import uz.apartmentForRent.projection.CustomRegion;

import java.util.UUID;

@RepositoryRestResource(path = "region", collectionResourceRel = "list", excerptProjection = CustomRegion.class)
public interface RegionRepository extends JpaRepository<Region, UUID> {
}
