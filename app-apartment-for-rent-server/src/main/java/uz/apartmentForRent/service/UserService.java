package uz.apartmentForRent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.apartmentForRent.entity.User;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.payload.CheckPassword;
import uz.apartmentForRent.payload.UserDto;
import uz.apartmentForRent.repository.UserRepository;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse addSellerOrUser(UserDto userDto, boolean seller) {
        try {
            if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber()))
                return new ApiResponse("Phone number is already exist in data base", HttpStatus.CONFLICT.value());
            userRepository.save(authService.makeUser(userDto, seller));
            return new ApiResponse("Successfully added", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.CONFLICT.getReasonPhrase());
        }
    }

    public ApiResponse editSellerOrUser(UserDto userDto) {
        try {
            Optional<User> optionalUser = userRepository.findById(userDto.getId());
            if (!optionalUser.isPresent())
                return new ApiResponse("User not found", HttpStatus.CONFLICT.value());
            User user = optionalUser.get();
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setPhoneNumber(userDto.getPhoneNumber());
            if (user.getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_SUPER_ADMIN") || user.getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_ADMIN"))
                user.setCompanyPhoneNumber(userDto.getCompanyPhoneNumber());
            userRepository.save(user);
            return new ApiResponse("Successfully edited", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.CONFLICT.getReasonPhrase());
        }
    }

    public ApiResponse addAdmin(UserDto userDto) {
        try {
            if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber()))
                return new ApiResponse("Phone number is already exist in data base", HttpStatus.CONFLICT.value());
            userRepository.save(authService.makeUser(userDto, null));
            return new ApiResponse("Successfully added", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.CONFLICT.getReasonPhrase());
        }
    }

    public ApiResponse editAdmin(UserDto userDto) {
        try {
            Optional<User> optionalUser = userRepository.findById(userDto.getId());
            if (!optionalUser.isPresent())
                return new ApiResponse("User not found", HttpStatus.CONFLICT.value());
            User user = optionalUser.get();
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setPhoneNumber(userDto.getPhoneNumber());
            userRepository.save(user);
            return new ApiResponse("Successfully edited", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.CONFLICT.getReasonPhrase());
        }
    }

    public ApiResponse editPassword(User user, CheckPassword checkPassword) {
//        System.out.println(user.getPassword());
//        System.out.println(passwordEncoder.encode(checkPassword.getOldPassword()));
//
//        System.out.println(passwordEncoder.matches(checkPassword.getOldPassword(), user.getPassword()));
//        System.out.println(passwordEncoder.matches(user.getPassword(),checkPassword.getOldPassword()));
        if (passwordEncoder.matches(checkPassword.getOldPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(checkPassword.getNewPassword()));
            userRepository.save(user);
            return new ApiResponse("Password edited", HttpStatus.OK.getReasonPhrase());
        }
        return new ApiResponse("Invalid old password", HttpStatus.CONFLICT.value());
    }


}
