package uz.apartmentForRent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.apartmentForRent.entity.User;
import uz.apartmentForRent.entity.enums.RoleName;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.payload.UserDto;
import uz.apartmentForRent.repository.RoleRepository;
import uz.apartmentForRent.repository.UserRepository;

import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;


    public UserDetails getUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("getUser"));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(username).orElseThrow(() -> new UsernameNotFoundException("Don't find phone number or change phone number"));
    }

//    public ApiResponse registerUser(UserDto userDto) {
//        try {
//            ApiResponse checkPassword = checkPassword(userDto);
//            if (!checkPassword.isSuccess()) {
//                return checkPassword;
//            }
//            userRepository.save(makeUser(userDto, false));
//            return new ApiResponse("Successfully added", HttpStatus.OK.getReasonPhrase());
//        } catch (Exception e) {
//            return new ApiResponse("Failed to save user", HttpStatus.CONFLICT.value());
//        }
//    }

    /**
     * Parolini tekshirish uchun method
     */
//    public ApiResponse checkPassword(UserDto userDto) {
//        if (userDto.getPassword().length() < 6 || userDto.getPassword().length() > 10)
//            return new ApiResponse("Password size is must be between 6 and 10 character!", HttpStatus.CONFLICT.value());
//        if (!(userDto.getPassword().equals(userDto.getPrePassword())))
//            return new ApiResponse("Password and pre-password is not equals!", HttpStatus.CONFLICT.value());
//        return new ApiResponse("OK", HttpStatus.OK.value());
//    }

    public User makeUser(UserDto userDto, Boolean seller) {
        return new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPhoneNumber(),
                passwordEncoder.encode(userDto.getPassword()),
                roleRepository.findAllByRoleName(seller == null ? RoleName.ROLE_ADMIN : seller ? RoleName.ROLE_SELLER : RoleName.ROLE_USER),
                true
        );
    }
}
