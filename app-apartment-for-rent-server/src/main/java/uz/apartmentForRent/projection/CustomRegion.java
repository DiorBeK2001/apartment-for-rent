package uz.apartmentForRent.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.apartmentForRent.entity.District;
import uz.apartmentForRent.entity.Region;

import java.util.UUID;

@Projection(name = "customRegion", types = Region.class)
public interface CustomRegion {
    UUID getId();

    String getName();

    boolean isActive();

    String getDescription();
}
