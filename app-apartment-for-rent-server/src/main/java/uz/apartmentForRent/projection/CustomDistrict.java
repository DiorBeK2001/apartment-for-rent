package uz.apartmentForRent.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.apartmentForRent.entity.Category;
import uz.apartmentForRent.entity.District;

import java.util.UUID;

@Projection(name = "customDistrict", types = District.class)
public interface CustomDistrict {
    UUID getId();

    UUID getRegionId();

    String getName();

    boolean isActive();

    String getDescription();
}
