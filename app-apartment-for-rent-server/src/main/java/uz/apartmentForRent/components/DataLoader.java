package uz.apartmentForRent.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.apartmentForRent.entity.*;
import uz.apartmentForRent.entity.enums.RoleName;
import uz.apartmentForRent.repository.*;

import java.text.SimpleDateFormat;
import java.util.HashSet;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    RegionRepository regionRepository;

    @Value("${spring.datasource.initialization}")
    private String initMode;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            roleRepository.save(new Role(1, RoleName.ROLE_SUPER_ADMIN));
            roleRepository.save(new Role(2, RoleName.ROLE_ADMIN));
            roleRepository.save(new Role(3, RoleName.ROLE_SELLER));
            roleRepository.save(new Role(4, RoleName.ROLE_USER));

            Category category = new Category();
            category.setName("student dehqon kk");
            category.setActive(true);
            category.setDescription("dehqon student kk");
            categoryRepository.save(category);

            Region region = new Region();
            region.setName("Toshkent");
            region.setActive(true);
            Region savedRegion = regionRepository.save(region);

            District district = new District();
            district.setName("Mirobod");
            district.setActive(true);
            district.setRegion(savedRegion);
            District savedDistrict = districtRepository.save(district);


            Address address = new Address();
            address.setName("Ketmonobod");
            address.setLat(41.29661299244055);
            address.setLon(69.27200253442413);
            address.setDistrict(savedDistrict);
            addressRepository.save(address);

            userRepository.save(
                    new User(
                            "SuperAdmin",
                            "Adminov",
                            "+998901234567",
                            passwordEncoder.encode("123"),
                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_SUPER_ADMIN)),
                            true
                    )
            );
            userRepository.save(
                    new User(
                            "SuperAdmin",
                            "Adminov",
                            "+998901234567",
                            passwordEncoder.encode("123"),
                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_SUPER_ADMIN)),
                            true
                    )
            );
        }
    }
}
